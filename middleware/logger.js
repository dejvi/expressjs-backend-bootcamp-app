//@desc    Logs request to console
const logger = (req, res, next) => {
  console.log('logger middleware ran');
  next();
};

module.exports = logger;
